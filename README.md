# :wave: Hi, my name is Pavel
## I am Full-stack web developer

Passionate developer with a strong commitment to effective solutions, clear documentation and transparent code. 

- :trophy: I'm a lifelong learner, always seeking to deepen my understanding of how things work;
- :construction_worker: Inspired to help people reaching their goals via outstanding digital tools.

## Skills

### Tech-stack

![C#](https://gitlab.com/ffxor/ffxor/-/raw/main/img/csharp.png) ![.NET Core](https://gitlab.com/ffxor/ffxor/-/raw/main/img/dotnet-core.png) ![Xamarin](https://gitlab.com/ffxor/ffxor/-/raw/main/img/xamarin.png) ![html5](https://gitlab.com/ffxor/ffxor/-/raw/main/img/html5.png) ![css3](https://gitlab.com/ffxor/ffxor/-/raw/main/img/css3.png) ![Sass](https://gitlab.com/ffxor/ffxor/-/raw/main/img/sass.png) ![JavaScript](https://gitlab.com/ffxor/ffxor/-/raw/main/img/javascript.png) ![TypeScript](https://gitlab.com/ffxor/ffxor/-/raw/main/img/typescript.png) ![React](https://gitlab.com/ffxor/ffxor/-/raw/main/img/react.png) ![Next,js](https://gitlab.com/ffxor/ffxor/-/raw/main/img/nextjs.png) ![Node.js](https://gitlab.com/ffxor/ffxor/-/raw/main/img/nodejs.png) ![Vite](https://gitlab.com/ffxor/ffxor/-/raw/main/img/vite.png) ![Figma](https://gitlab.com/ffxor/ffxor/-/raw/main/img/figma.png) 

### DB

![PostgreSQL](https://gitlab.com/ffxor/ffxor/-/raw/main/img/postgresql.png) ![MySQL](https://gitlab.com/ffxor/ffxor/-/raw/main/img/mysql.png) ![MSSQL](https://gitlab.com/ffxor/ffxor/-/raw/main/img/mssql.png)

### DevOps

![GitLab](https://gitlab.com/ffxor/ffxor/-/raw/main/img/gitlab.png) ![Docker](https://gitlab.com/ffxor/ffxor/-/raw/main/img/docker.png) ![Kubernetes](https://gitlab.com/ffxor/ffxor/-/raw/main/img/kubernetes.png) ![Digital Ocean](https://gitlab.com/ffxor/ffxor/-/raw/main/img/digital-ocean.png) ![nginx](https://gitlab.com/ffxor/ffxor/-/raw/main/img/nginx.png) ![Linux](https://gitlab.com/ffxor/ffxor/-/raw/main/img/linux.png) 

## Enthusiastic

Fascinated by

![Mikrotik](https://gitlab.com/ffxor/ffxor/-/raw/main/img/mikrotik.png) ![Arduino](https://gitlab.com/ffxor/ffxor/-/raw/main/img/arduino.png)

## Learning

Currently expanding my knowledge horizon in

![Python](https://gitlab.com/ffxor/ffxor/-/raw/main/img/python.png) ![Swift](https://gitlab.com/ffxor/ffxor/-/raw/main/img/swift.png) ![Kotlin](https://gitlab.com/ffxor/ffxor/-/raw/main/img/kotlin.png)

## Social

Keep in contact

[![linked-in](https://gitlab.com/ffxor/ffxor/-/raw/main/img/linked-in.png)](https://www.linkedin.com/in/pavel-dotnet-petrov/) [![Telegram](https://gitlab.com/ffxor/ffxor/-/raw/main/img/telegram.png)](https://t.me/ffxor)